import pygame
import random

bijelaboja	= (255, 255, 255)
crna = (0, 0, 0)
pozadina	= (213, 213, 213)
size = (600, 450)

screen = pygame.display.set_mode(size)
pygame.display.set_caption("Memori igra - Emil Blažević")
pygame.font.init()
clock = pygame.time.Clock()


sve_karte = []
imena_slika = ["1", "tesla", "sl500", "d3", "otpornik", "controller", "delta"]
pozicijax = [100.0, 202.0, 304.0, 406.0, 100.0, 202.0, 304.0, 406.0, 100.0, 202.0, 304.0, 406.0, 100.0, 202.0]
pozicijay = [10.0, 10.0, 10.0, 10.0, 112.0, 112.0, 112.0, 112.0, 214.0, 214.0, 214.0, 214.0, 316.0, 316.0]
pogodjenekarte = []
otvorenekarte = []

def karte_provjera(k):
	if k[0][3] != k[1][3]:
		return 0
	else:
		pogodjenekarte.append(k[0])
		pogodjenekarte.append(k[1])
		return 1

def kreiraj_karte():
	i = 0
	for k in sve_karte:
		img = k[0]
		if k[2] == False:
			img = pygame.image.load("naslovna.jpg").convert()
			img = pygame.transform.scale(img, (100, 100))
		k[1] = screen.blit(img, (pozicijax[i], pozicijay[i]))
		i = i + 1

def preokrenute_karte(karte):
	brojac = 0
	for k in karte:
		if k[2] == False:
			brojac = brojac + 1
	return brojac

def rezultat():
	font = pygame.font.SysFont("Arial", 25, True)
	render = font.render("Rez: " + str(rezultat_igre), False, crna)
	screen.blit(render, (400.0, 350.0))
	font = pygame.font.SysFont("Arial", 18, True)
	render = font.render("Za prekid igre pritisnuti x!", False, crna)
	screen.blit(render, (350.0, 380.0))


rezultat_igre = 0
trenutno_stanje = "stanjekreiraj"
done = False
timer_cekanja = 1500

while not done:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True

		elif event.type == pygame.MOUSEBUTTONDOWN:
			if trenutno_stanje == "stanjeigra":
				for card in sve_karte:
					if card[1].collidepoint(event.pos):
						card[2] = True
						otvorenekarte.append(card)
						break

	screen.fill(pozadina)
	if trenutno_stanje == "stanjekreiraj":
		first = []
		for slika in imena_slika:
			img = pygame.image.load(slika + ".jpg").convert()
			img = pygame.transform.scale(img, (100, 100))
			rect = img.get_rect()
			first.append([img, rect, False, slika])

		second = []
		for image in first:
			img = pygame.image.load(image[3] + ".jpg").convert()
			img = pygame.transform.scale(img, (100, 100))
			rect = img.get_rect()
			second.append([img, rect, False, image[3]])

		sve_karte = first + second
		random.shuffle(sve_karte)
		trenutno_stanje = "stanjeigra"

	elif trenutno_stanje == "stanjeigra":
		kreiraj_karte()	
		rezultat()
		if len(otvorenekarte) == 2:
			timer_cekanja = 1500
			trenutno_stanje = "cekaj"


	elif trenutno_stanje == "cekaj":
		rezultat()
		kreiraj_karte()

		timer_cekanja -= clock.get_time()
		if timer_cekanja <= 0:
			trenutno_stanje = "provjerikarte"
			timer_cekanja = 1500

	elif trenutno_stanje == "provjerikarte":
		rezultat()
		kreiraj_karte()	

		if karte_provjera(otvorenekarte) == 0:
			for card in sve_karte:
				if card not in pogodjenekarte:
					card[2] = False
		del otvorenekarte[:]

		if preokrenute_karte(sve_karte) == 0:
			rezultat_igre = rezultat_igre + 1
			trenutno_stanje = "stanjekreiraj"
		else:
			trenutno_stanje = "stanjeigra"

	pygame.display.flip()
	clock.tick(60)